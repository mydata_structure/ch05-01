#include <stdio.h>
#include <stdlib.h>
#include "BiTNode.h"

int top=-1;

typedef struct SNode{
	BiTree p;
	int tag;
};

/*
创建一个二叉树
*/
void CreateBiTree(BiTree *T) {
	*T = (BitNode*)malloc(sizeof(BitNode));

	(*T)->data=1;
	(*T)->lchild=(BitNode*)malloc(sizeof(BitNode));
	(*T)->rchild=NULL;

	(*T)->lchild->data=2;
	(*T)->lchild->lchild= (BitNode*)malloc(sizeof(BitNode));
	(*T)->lchild->rchild=NULL;


	(*T)->lchild->lchild->data=3;
	(*T)->lchild->lchild->lchild=NULL;
	(*T)->lchild->lchild->rchild=NULL;


}

void CreateBiTree1(BiTree *T){
	*T = (BitNode*)malloc(sizeof(BitNode));
	(*T)->data=1;
	(*T)->lchild=(BitNode*)malloc(sizeof(BitNode));
	(*T)->rchild=(BitNode*)malloc(sizeof(BitNode));
	(*T)->lchild->data=2;
	
	(*T)->lchild->lchild = (BitNode*)malloc(sizeof(BitNode));
	(*T)->lchild->rchild =(BitNode*)malloc(sizeof(BitNode));
	(*T)->lchild->rchild->data=5;
	
	(*T)->lchild->rchild->lchild=NULL; 
	(*T)->lchild->rchild->rchild=NULL; 
	(*T)->rchild->data=3;
	(*T)->rchild->lchild = (BitNode*)malloc(sizeof(BitNode));
	(*T)->rchild->lchild->data=6;
	
	(*T)->rchild->lchild->lchild=NULL;
	(*T)->rchild->lchild->rchild=NULL;

	(*T)->rchild->rchild=(BitNode*)malloc(sizeof(BitNode));
	(*T)->rchild->rchild->data=7;
	(*T)->rchild->rchild->lchild=NULL;
	(*T)->rchild->rchild->rchild=NULL;
	(*T)->lchild->lchild->data=4;
	(*T)->lchild->lchild->lchild=NULL;
	(*T)->lchild->lchild->rchild=NULL; 
}

/*//模拟操作结点元素的函数，输出结点本身的数值*/
void displayElem(BitNode* elem){
	printf("%d",elem->data);
}

//先序遍历
void PreOrderTraverse(BiTree T){
	if(T){
		//调用操作节点数据的函数方法 
		displayElem(T);
		PreOrderTraverse(T->lchild);//访问该节点的左孩子
		PreOrderTraverse(T->rchild);//访问该节点的右孩子 
	}
	//如果节点为空，返回上一层
	return; 
}


//中序遍历
void INOrderTraverse(BiTree T){
	if(T){
		INOrderTraverse(T->lchild);//遍历左孩子
		displayElem(T);
		INOrderTraverse(T->rchild);//遍历右孩子 
	}
	return;
}

//后序遍历
void PostOrderTraverse(BiTree T){
	if(T){
		PostOrderTraverse(T->lchild);//遍历左孩子
		PostOrderTraverse(T->rchild);//遍历右孩子
		displayElem(T); 
	}
	//如果节点为空，返回上一层
	return; 
}

// 先序遍历非递归算法
void PreOrderTraverse1(BiTree Tree){
	BitNode *a[20];//定义一个顺序栈
	BitNode *p;//临时指针
	push(*a,Tree);
	while(top!=-1){
		p=getTop(a);//获取栈顶元素
		pop();//弹栈
		while(p){
			displayElem(p);//调用节点的操作函数
			//如果该节点有右孩子，右孩子进栈 
			if(p->rchild){
				push(a,p->rchild);
			} 
			p=p->lchild;//一直指向根节点的最后一个左孩子 
		} 
	}
}

//中序遍历非递归算法 
void INOrderTraverse1(BiTree Tree){
	BitNode *a[20];//定义一个顺序栈
	BitNode *p;//临时指针
	p=Tree; 
	//当p为NUll或者栈为空时，将其压栈并遍历其左子树 
	while(p||top!=-1){
		if(p){
			push(a,p);
			p=p->lchild;
		} else{//如果p=NULL,表明左子树遍历完成，需要遍历上一层节点的右子树
		  p=getTop(a); 
		  pop();
		  displayElem(p);
		  p=p->rchild;
	 		
		}
	} 
	
}

//后序遍历非递归算法 
void PostOrderTraverse1(BiTree Tree){
	
	SNode a[20];//定义一个顺序栈
	BitNode *p;//临时指针
	
	int tag;
	SNode sdata;
	p=Tree;
	while(p||top!=-1){
		while(p){
			//为该节点入栈做准备
			sdata.p=p;
			sdata.tag=0;//由于是遍历左孩子，设置标志位为0
			postpush(a,sdata);//压栈 
			p=p->lchild;//以该结点为根结点，遍历左孩子
		}
		sdata = a[top];//获取栈顶元素
		pop();//弹出栈顶元素
		p=sdata.p; 
		tag=sdata.tag;
		//如果tag=0，说明该节点还没有遍历他的右孩子
		if(tag==0){
			sdata.p=p;
			sdata.tag=1;
			postpush(a,sdata);//更改该节点的标志位，重新压栈
			p=p->rchild;//以该节点的右孩子为根节点，重复循环 
		} else{//如果取出来的栈顶元素tag=1,说明此节点左右子树都遍历完了，可以调用打印函数了
			displayElem(p);
			p=NULL;
		} 
	}
	
	
}
/*
后序遍历用到的进栈函数 
*/
void postpush(SNode*a,SNode sdata){
	a[++top]=sdata;
}

/*
前序和中序遍历使用的进栈函数 
*/
void push(BitNode **a,BitNode *elem){
	a[++top]=elem;
}

/**
弹栈函数 
*/
void pop(){
	if(top==-1){
		return;
	}
	top--;
} 
/*
拿到栈顶元素 
*/
BitNode getTop(BitNode **a){
	return a[top];
}


