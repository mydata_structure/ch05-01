#include <stdio.h>
#include <stdlib.h>
#define TElemType int


typedef struct BitNode{
	TElemType data;//数据域
	struct BitNode *lchild,*rchild;//左右孩子指针 
}BitNode,*BiTree;

/*
创建一个二叉树 
*/
void CreateBiTree(BiTree *T);

void CreateBiTree1(BiTree *T);

/*//模拟操作结点元素的函数，输出结点本身的数值*/
void displayElem(BitNode* elem);

//先序遍历
void PreOrderTraverse(BiTree T);


//中序遍历
void INOrderTraverse(BiTree T);

//后序遍历
void PostOrderTraverse(BiTree T);

/*==================遍历的非递归算法===================*/

// 先序遍历非递归算法
/*
从树的根节点出发，遍历左孩子的同时，先将每个节点的右孩子压栈，当遇到节点没有左孩子的时候，取栈顶的右孩子，
重复以上过程 
*/
void PreOrderTraverse1(BiTree Tree);

//中序遍历
/*
从根节点开始，遍历左孩子同时压栈，当遍历结束，说明当前遍历的节点没有左孩子，
从栈中取出来调用函数，然后访问该节点的右孩子，继续以上重复性的操作 
*/ 
void INOrderTraverse1(BiTree T);

//后序遍历
/*
后序遍历是在遍历完当前节点的左右孩子后，才调用函数，所以需要在操作节点进栈时，
为每个节点配备一个标志位，当遍历该节点的左孩子时，设置当前节点标志位0，进栈；
当要遍历该节点的右孩子时，设置当前节点的标志位为1，进栈，
这样，当遍历完成，该节点弹出，查看该节点的标志位的值，如果是0，表示该节点的右孩子还没有遍历，反之，如果是1
说明该节点的左右孩子都遍历完成，可以调用该函数 
*/ 
void PostOrderTraverse1(BiTree T);







