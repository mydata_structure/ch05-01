#include <stdio.h>
#include <stdlib.h>

#include "BiTNode.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {

	BiTree tree;
	CreateBiTree1(&tree);
	printf("%d\n",tree->lchild->lchild->data);

//	printf("前序遍历:\n");
//	PreOrderTraverse(tree);
//
//	printf("\n中序遍历:\n");
//	INOrderTraverse(tree);
//
//	printf("\n后序遍历:\n");
//	PostOrderTraverse(tree);



	printf("非递归的前序遍历:\n");
	PreOrderTraverse1(tree);


	return 0;
}
